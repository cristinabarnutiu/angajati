package AngajatiApp.controller;

import java.util.List;

import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import AngajatiApp.view.EmployeeView;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeView employeeView;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
		this.employeeView = new EmployeeView();
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, DidacticFunction newFunction) {
		employeeRepository.modifyEmployeeFunction(oldEmployee, newFunction);
	}
	
	public List<Employee> getSortedEmployeeList() {
		return employeeRepository.getEmployeeByCriteria();
	}
	
	public void printMenu() {
		employeeView.printMenu();
	}

	public Employee findEmployeeById(int idOldEmployee) {
		return employeeRepository.findEmployeeById(idOldEmployee);
	}


	private int comparebyAge (Employee o1, Employee o2) {
		int employee1Age = Integer.parseInt(o1.getCnp().substring(1,7));
		int employee2Age = Integer.parseInt(o2.getCnp().substring(1,7));
		return employee2Age - employee1Age;
	}

	private int comparebySalary(Employee o1, Employee o2) {
		return (int) (o2.getSalary() - o1.getSalary());
	}

}

