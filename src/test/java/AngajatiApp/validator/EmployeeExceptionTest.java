package AngajatiApp.validator;

import AngajatiApp.model.Employee;
import org.junit.*;

import static org.junit.Assert.*;

public class EmployeeExceptionTest {

    private Employee e1;
    private Employee e2;
    private Employee e3;
    private Employee e4;

    //se executa automat inaintea fiecarei metode de test
    @Before
    public void setUp() {
        e1 = new Employee(); //"Ionel Popescu",123,test,1000,0);
        e1.setFirstName("Ionel");
        e2 = new Employee(); //"Andrei Ionescu",123,test,1000,1);
        e2.setFirstName("Andrei");
        e2.setLastName("Ionescu");
        e2.setCnp("1244567890876");
        e3 = new Employee();
        e4 = null;
        System.out.println("------------------");
        System.out.println("executing test:");

    }

    //se executa dupa fiecare metoda de test
    @After
    public void tearDown() {
        e1 = null;
        e2 = null;
        System.out.println("test finished");
        System.out.println("------------------");
    }
    //@Ignore
    @Test
    public void testGetFirstName() {
        assertEquals("Ionel", e1.getFirstName());
        System.out.println("test GetFirstName");
    }

    //@Ignore
    @Test
    public void testGetLastName() {
        assertEquals("Ionescu", e2.getLastName());
        System.out.println("test GetLastName");
    }

    //@Ignore
    @Test
    public void testSetFirstName() {
        e2.setFirstName("Gabriel");
        assertEquals("Gabriel", e2.getFirstName());
        System.out.println("test SetFirstName");
        System.out.println("FirstName has been successfully changed to");
        System.out.println(e2.getFirstName());
    }

    //@Ignore
    @Test
    public void testConstructor() {
        System.out.println("test Constructor");
        assertNotEquals("Verificam daca s-a creat angajatul 1", e1, null);  //verifica daca e1 este diferit de null, adica nu este egal cu null
    }

    //@Ignore
    @Test (expected=NullPointerException.class)
    public void testGetFirstName3(){
        System.out.println("test GetFirstName3");
        assertEquals("ceva", e4.getFirstName());

    }

    @Test(timeout = 100) //asteapta 10 milisecunde, daca dureaza mai mult de 10 milisecunde crapa, daca dureaza
    public void testFictiv() {
        try {
            Thread.sleep(50);
            System.out.println("test fictiv");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public static void setUpAll() {
        System.out.println("------------------");
        System.out.println("START TEST SUITE");
        System.out.println("------------------");
    }

    @AfterClass
    public static void tearDownAll() {
        System.out.println("------------------");
        System.out.println("END TEST SUITE");
        System.out.println("------------------");
    }

}